<?php
namespace DluTwBootstrap\Form\View\Helper;

use DateTime;
use IntlDateFormatter;
use Zend\Form\ElementInterface;
use Zend\Form\Element\DateSelect as DateSelectElement;
use Zend\Form\Exception;
use DluTwBootstrap\Form\View\Helper\FormMonthSelectTwb as FormMonthSelectHelper;

class FormDateSelectTwb extends FormMonthSelectHelper
{
    /**
     * Render a date element that is composed of three selects
     *
     * @param  ElementInterface $element
     * @throws \Zend\Form\Exception\InvalidArgumentException
     * @throws \Zend\Form\Exception\DomainException
     * @return string
     */
    public function render(ElementInterface $element, $formType = null, array $displayOptions = array())
    {
        if (!$element instanceof DateSelectElement) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s requires that the element is of type Zend\Form\Element\DateSelect',
                __METHOD__
            ));
        }

        $name = $element->getName();
        if ($name === null || $name === '') {
            throw new Exception\DomainException(sprintf(
                '%s requires that the element has an assigned name; none discovered',
                __METHOD__
            ));
        }

        // Allow passing of options to each of the rendered elements
        if (!array_key_exists('day', $displayOptions)) {
            $displayOptions['day'] = array();
        }
        if (!array_key_exists('month', $displayOptions)) {
            $displayOptions['month'] = array();
        }
        if (!array_key_exists('year', $displayOptions)) {
            $displayOptions['year'] = array();
        }

        $selectHelper = $this->getSelectElementHelper();
        $pattern      = $this->parsePattern($element->shouldRenderDelimiters());

        $daysOptions   = $this->getDaysOptions($pattern['day']);
        $monthsOptions = $this->getMonthsOptions($pattern['month']);
        $yearOptions   = $this->getYearsOptions($element->getMinYear(), $element->getMaxYear());

        $dayElement   = $element->getDayElement()->setValueOptions($daysOptions);
        $monthElement = $element->getMonthElement()->setValueOptions($monthsOptions);
        $yearElement  = $element->getYearElement()->setValueOptions($yearOptions);

        if ($element->shouldCreateEmptyOption()) {
            $dayElement->setEmptyOption('');
            $yearElement->setEmptyOption('');
            $monthElement->setEmptyOption('');
        }
        
        $dayElement->setAttribute('class', 'select-day');
        $monthElement->setAttribute('class', 'select-month');
        $yearElement->setAttribute('class', 'select-year');

        $data = array();
        $data[$pattern['day']]   = $selectHelper->render($dayElement, $formType, $displayOptions['day']);
        $data[$pattern['month']] = $selectHelper->render($monthElement, $formType, $displayOptions['month']);
        $data[$pattern['year']]  = $selectHelper->render($yearElement, $formType, $displayOptions['year']);


        $markup = '';
        foreach ($pattern as $key => $value) {
            // Delimiter
            if (is_numeric($key)) {
                $markup .= $value;
            } else {
                $markup .= $data[$value];
            }
        }

        return $markup;
    }

    /**
     * Create a key => value options for days
     *
     * @param string  $pattern Pattern to use for days
     * @return array
     */
    protected function getDaysOptions($pattern)
    {
        $keyFormatter   = new IntlDateFormatter($this->getLocale(), null, null, null, null, 'dd');
        $valueFormatter = new IntlDateFormatter($this->getLocale(), null, null, null, null, $pattern);
        $date           = new DateTime('1970-01-01');

        $result = array();
        for ($day = 1; $day <= 31; $day++) {
            $key   = $keyFormatter->format($date->getTimestamp());
            $value = $valueFormatter->format($date->getTimestamp());
            $result[$key] = $value;

            $date->modify('+1 day');
        }

        return $result;
    }
}
